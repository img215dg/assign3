package ca.fniscak.assign3.activities;

import android.content.Context;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.raizlabs.android.dbflow.sql.language.Delete;
import com.raizlabs.android.dbflow.sql.language.Select;
import com.squareup.picasso.Picasso;

import java.io.BufferedReader;
import java.io.InputStream;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import ca.fniscak.assign3.R;
import ca.fniscak.assign3.models.Chocolate;
import ca.fniscak.assign3.models.Whiskey;

public class MainActivity extends AppCompatActivity {
    private String TAG = this.getClass().getSimpleName();
    Context context = this;
    ImageView imageView;
    int chocolatePos = 0;
    int whiskeyPos = 0;

    // to run repeated task
    private Handler handler = new Handler();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = (ImageView) findViewById(R.id.ivMainSlideShow);

        //List<Tea> list = new Select().from(Tea.class).queryList();

        List<Chocolate> chocolateList = new Select().from(Chocolate.class).queryList();
        List<Whiskey> whiskeyList = new Select().from(Whiskey.class).queryList();

        loadFile(imageView);

        if (chocolateList.isEmpty()) loadFile(imageView);
        if (whiskeyList.isEmpty()) loadFile(imageView);

     /* and here comes the "trick" to run repated task */
        handler.postDelayed(runnable, 2000);


    }

    public void loadFile(View view) {
        BufferedReader reader = null;
        InputStream inputStream = null;
        try {
            //inputStream = getResources().openRawResource(R.raw.green_tea);
            inputStream = getResources().openRawResource(R.raw.chocolate);
            Scanner scanner = new Scanner(inputStream);
            int i = 0;
            while (scanner.hasNextLine()) {
                i++;
                String line = scanner.nextLine();
                String[] parts = line.split("\\|");
                //Tea tea = new Tea();
                Chocolate chocolate = new Chocolate();
                //createTea(chocolate, parts);
                Log.e("DEBUG", "Calling createChocolate with " + parts.length + " parts");
                createChocolate(chocolate, parts, i);
                Log.e("DEBUG", "Image_front_url is " + chocolate.getImage_front_url());
                if (chocolate.getImage_front_url().contains("jpg")) {
                    Log.e("DEBUG", "Saving chocolate");
                    chocolate.save();
                }
            }

            inputStream = getResources().openRawResource(R.raw.whiskey);
            scanner = new Scanner(inputStream);
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                String[] parts = line.split("\\|");
                //Tea tea = new Tea();
                Whiskey whiskey = new Whiskey();
                //createTea(chocolate, parts);
                createWhiskey(whiskey, parts);
                if (whiskey.getImage_front_url().contains("jpg")) {
                    whiskey.save();
                }
            }

        } catch (Exception e) {
            e.getMessage();
        } finally {

        }
    }

//  public void createTea(Tea teaItem, String[] parts)
//  {
//    final int CODE = 0;
//    if (parts.length > CODE)
//      teaItem.setCode(parts[CODE]);
//    else
//      teaItem.setCode("NO CODE");
//
//    final int URL = 1;
//    if (parts.length > URL)
//      teaItem.setUrl(parts[URL]);
//    else
//      teaItem.setUrl("NO URL");
//
//    final int CREATOR = 2;
//    if (parts.length > CREATOR)
//      teaItem.setCreator(parts[CREATOR]);
//    else
//      teaItem.setCreator("NO CREATOR");
//
//    final int PRODUCT_NAME = 5;
//    if (parts.length > PRODUCT_NAME)
//      teaItem.setProduct_name(parts[PRODUCT_NAME]);
//    else
//      teaItem.setProduct_name("NO CREATOR");
//
//    final int BRANDS = 10;
//    if (parts.length > BRANDS)
//      teaItem.setBrands(parts[BRANDS]);
//    else
//      teaItem.setBrands("NO BRANDS");
//
//    //final int IMAGE_FRONT_URL = 48;
//    final int IMAGE_FRONT_URL = 47;
//    if (parts.length > IMAGE_FRONT_URL)
//      teaItem.setImage_front_url(parts[IMAGE_FRONT_URL]);
//    else
//      teaItem.setImage_front_url("NO IMAGE FRONT URL");
//
//
//    Log.e(TAG, "Number of Parts : " + parts.length);
//    Log.e(TAG, "CRETE TEA - END : " + teaItem.toString());
//  }


    public void createChocolate(Chocolate chocolateItem, String[] parts, int id) {
        final int CODE = 0;
        if (parts.length > CODE)
            chocolateItem.setCode(parts[CODE]);
        else
            chocolateItem.setCode("NO CODE");

        final int URL = 1;
        if (parts.length > URL)
            chocolateItem.setUrl(parts[URL]);
        else
            chocolateItem.setUrl("NO URL");

        final int CREATOR = 2;
        if (parts.length > CREATOR)
            chocolateItem.setCreator(parts[CREATOR]);
        else
            chocolateItem.setCreator("NO CREATOR");

        final int PRODUCT_NAME = 5;
        if (parts.length > PRODUCT_NAME)
            chocolateItem.setProduct_name(parts[PRODUCT_NAME]);
        else
            chocolateItem.setProduct_name("NO CREATOR");

        final int BRANDS = 10;
        if (parts.length > BRANDS)
            chocolateItem.setBrands(parts[BRANDS]);
        else
            chocolateItem.setBrands("NO BRANDS");

        //final int IMAGE_FRONT_URL = 48;
        final int IMAGE_FRONT_URL = 47;
        if (parts.length > IMAGE_FRONT_URL)
            chocolateItem.setImage_front_url(parts[IMAGE_FRONT_URL]);
        else
            chocolateItem.setImage_front_url("NO IMAGE FRONT URL");

        chocolateItem.setId(id);

        Log.e(TAG, "Number of Parts : " + parts.length);
        Log.e(TAG, "CREATE CHOCOLATE - END : " + chocolateItem.toString());
    }

    public void createWhiskey(Whiskey whiskeyItem, String[] parts) {
        final int CODE = 0;
        if (parts.length > CODE)
            whiskeyItem.setCode(parts[CODE]);
        else
            whiskeyItem.setCode("NO CODE");

        final int URL = 1;
        if (parts.length > URL)
            whiskeyItem.setUrl(parts[URL]);
        else
            whiskeyItem.setUrl("NO URL");

        final int CREATOR = 2;
        if (parts.length > CREATOR)
            whiskeyItem.setCreator(parts[CREATOR]);
        else
            whiskeyItem.setCreator("NO CREATOR");

        final int PRODUCT_NAME = 5;
        if (parts.length > PRODUCT_NAME)
            whiskeyItem.setProduct_name(parts[PRODUCT_NAME]);
        else
            whiskeyItem.setProduct_name("NO CREATOR");

        final int BRANDS = 10;
        if (parts.length > BRANDS)
            whiskeyItem.setBrands(parts[BRANDS]);
        else
            whiskeyItem.setBrands("NO BRANDS");

        //final int IMAGE_FRONT_URL = 48;
        final int IMAGE_FRONT_URL = 47;
        if (parts.length > IMAGE_FRONT_URL)
            whiskeyItem.setImage_front_url(parts[IMAGE_FRONT_URL]);
        else
            whiskeyItem.setImage_front_url("NO IMAGE FRONT URL");


        Log.e(TAG, "Number of Parts : " + parts.length);
        Log.e(TAG, "CREATE WHISKEY - END : " + whiskeyItem.toString());
    }

    public void emptyDatabaseTable(View view) {
        //List<Tea> list = new Select().from(Tea.class).queryList();
        List<Chocolate> chocolateList = new Select().from(Chocolate.class).queryList();
        List<Whiskey> whiskeyList = new Select().from(Whiskey.class).queryList();

        //Log.e(TAG, "BEFORE TABLE CLEAR: Number of Teas in Database : " + list.size());
        Log.e(TAG, "BEFORE TABLE CLEAR: Number of Chocolates in Database : " + chocolateList.size());
        Log.e(TAG, "BEFORE TABLE CLEAR: Number of Whiskeys in Database : " + whiskeyList.size());
        //new Delete().from(Tea.class).where().query();
        new Delete().from(Chocolate.class).where().query();
        new Delete().from(Whiskey.class).where().query();

        //list = new Select().from(Tea.class).queryList();
        chocolateList = new Select().from(Chocolate.class).queryList();
        whiskeyList = new Select().from(Whiskey.class).queryList();

        //Log.e(TAG, "AFTER TABLE CLEAR: Number of Teas in Database : " + list.size());
        Log.e(TAG, "AFTER TABLE CLEAR: Number of Chocolates in Database : " + chocolateList.size());
        Log.e(TAG, "AFTER TABLE CLEAR: Number of Whiskeys in Database : " + whiskeyList.size());
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            String imageURL = "";
            //Tea tea = new Tea();
            Chocolate chocolate = new Chocolate();
            Whiskey whiskey = new Whiskey();

            //List<Tea> list = new Select().from(Tea.class).queryList();
            List<Chocolate> chocolateList = new Select().from(Chocolate.class).queryList();
            List<Whiskey> whiskeyList = new Select().from(Whiskey.class).queryList();

//      if (list.size() > 2)
//      {
//        tea = list.get(pos);
//        imageURL = tea.getImage_front_url();
//        if (!imageURL.contains("http"))
//          imageURL = "https://static.openfoodfacts" +
//              ".org/images/products/350/211/000/7537/front_fr.10.200.jpg";
//
//        pos = (pos + 1) % list.size();
//      }

            // Randomly pick between displaying an image of Whiskey or of Chocolate
            Random rand = new Random();
            int i = rand.nextInt(100);
            Log.e("DEBUG", "i is " + i);
            if (i > 50) {
                Log.e("DEBUG", "chocolateList.size() = " + chocolateList.size());
                if (chocolateList.size() > 2) {
                    Log.e("DEBUG", "Picking a Chocolate image");
                    chocolate = chocolateList.get(chocolatePos);
                    imageURL = chocolate.getImage_front_url();
                    if (!imageURL.contains("http")) {
                        Log.e("DEBUG", "Picking DEFUALT Chocolate image");
                        imageURL = "https://static.openfoodfacts" +
                                ".org/images/products/841/000/081/0004/front_fr.41.400.jpg";
                    }

                    chocolatePos = (chocolatePos + 1) % chocolateList.size();
                }
            } else {
                Log.e("DEBUG", "whiskeyList.size() = " + whiskeyList.size());
                if (whiskeyList.size() > 2) {
                    Log.e("DEBUG", "Picking a Whiskey image");
                    whiskey = whiskeyList.get(whiskeyPos);
                    imageURL = whiskey.getImage_front_url();
                    if (!imageURL.contains("http")) {
                        Log.e("DEBUG", "Picking DEFUALT Whiskey image");
                        imageURL = "https://static.openfoodfacts" +
                                ".org/images/products/309/987/304/5864/front_fr.5.400.jpg";
                    }

                    whiskeyPos = (whiskeyPos + 1) % whiskeyList.size();
                }
            }
            // load the image
            Log.e("DEBUG", "Just before Loading Image" + imageURL);
            if (imageURL.contains("http")) {
                Log.e("DEBUG", "Loading Image");
                Picasso.with(context).load(imageURL).into(imageView);
                Log.e("DEBUG", "Loaded Image with url " + imageURL);
            }

      /* and here comes the "trick" to restart the periodic task again */
            handler.postDelayed(this, 2000);
        }
    };

    // Clicked stop slideshow button - remove the Handler callback to stop loading new images
    public void clickedStop(View view) {
        handler.removeCallbacks(runnable);
    }

    // Clicked restart slideshow button - set the Handler callback to load a new image every two seconds
    public void clickedRestart(View view) {
        handler.postDelayed(runnable, 2000);
    }
    // Clicked reload database button - empty database then load data in again
    public void clickedReload(View view) {
        emptyDatabaseTable(view);
        loadFile(view);
    }
}
