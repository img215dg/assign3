package ca.fniscak.assign3.models;

import com.raizlabs.android.dbflow.annotation.Database;

/**
 * Created by frank on 2018-02-02.
 */

@Database(name = MyDatabase.NAME, version = MyDatabase.VERSION)
public class MyDatabase {
  public static final String NAME = "MyDataBase";
  public static final int VERSION = 2;
}
