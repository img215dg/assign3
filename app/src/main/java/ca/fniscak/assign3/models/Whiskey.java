package ca.fniscak.assign3.models;

import android.provider.ContactsContract;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ForeignKey;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by Gage on 2/5/2018.
 */

@Table(database = MyDatabase.class)
public class Whiskey extends BaseModel
{
    @Column
    @PrimaryKey(autoincrement = true)
    int id;

    @Column
    String code;

    @Column
    String url;

    @Column
    String creator;

    @Column
    String product_name;

    @Column
    String brands;

    @Column
    String image_front_url;

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getCreator()
    {
        return creator;
    }

    public void setCreator(String creator)
    {
        this.creator = creator;
    }

    public String getProduct_name()
    {
        return product_name;
    }

    public void setProduct_name(String product_name)
    {
        this.product_name = product_name;
    }

    public String getBrands()
    {
        return brands;
    }

    public void setBrands(String brands)
    {
        this.brands = brands;
    }

    public String getImage_front_url()
    {
        return image_front_url;
    }

    public void setImage_front_url(String image_front_url)
    {
        this.image_front_url = image_front_url;
    }


    @Override
    public String toString()
    {
        return "Whiskey{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", url='" + url + '\'' +
                ", creator='" + creator + '\'' +
                ", product_name='" + product_name + '\'' +
                ", brands='" + brands + '\'' +
                ", image_front_url='" + image_front_url + '\'' +
                '}';
    }
}
